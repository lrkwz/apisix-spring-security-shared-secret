package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class HealthCheckControllerIntegrationTest {

    private static final String HEALTH_CHECK_ENDPOINT = "http://localhost:%s/health";

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int serverPort;

    @Test
    void givenApplicationIsRunning_whenHealthCheckControllerCalled_thenReturnOk() throws Exception {
        final HttpHeaders headers = new HttpHeaders();

        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(HEALTH_CHECK_ENDPOINT, serverPort)), HttpMethod.GET,
                new HttpEntity<>(headers), String.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("OK", response.getBody());
    }
}
