package com.example.demo;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.net.URI;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(classes = DemoApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class ApiControllerIntegrationTest {

    private static final String API_ENDPOINT = "http://localhost:%s/hello";

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @LocalServerPort
    private int serverPort;

    @Test
    void givenAuthHeaderIsInvalid_whenApiControllerCalled_thenReturnUnAuthorised() throws Exception {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("x-auth-secret-key", "invalid");

        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort)), HttpMethod.GET, new HttpEntity<>(headers), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    void givenAuthHeaderIsMissing_whenApiControllerCalled_thenReturnUnAuthorised() throws Exception {
        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort)), HttpMethod.GET, new HttpEntity<>(null), String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    void givenAuthHeaderNameIsInValid_whenApiControllerCalled_thenReturnUnAuthorised() throws Exception {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("x-auth-secret-key-wrong-name", "test-secret");

        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort)), HttpMethod.GET, new HttpEntity<>(headers), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    void givenAuthHeaderSecretIsValidAndRoleEditor_whenPostApiControllerCalled_then_ReturnOk() throws URISyntaxException {
        final HttpHeaders headers = getHttpHeaders();

        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort) + "?planetName=moon"), HttpMethod.POST, new HttpEntity<>(headers), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @NotNull
    private static HttpHeaders getHttpHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.add("x-auth-secret-key", "test-secret");
        return headers;
    }

    @Test
    void givenAuthHeaderSecretIsValid_whenApiControllerCalled_thenReturnOk() throws Exception {
        final HttpHeaders headers = getHttpHeaders();

        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort)), HttpMethod.GET, new HttpEntity<>(headers), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).startsWith("Hello ");
    }

    @Test
    void sinceUserIsNotAdmin_whenDeleteApiControllerCalled_then_ReturnFailure() throws URISyntaxException {
        final HttpHeaders headers = getHttpHeaders();
        final ResponseEntity<String> response = restTemplate.exchange(new URI(String.format(API_ENDPOINT, serverPort)), HttpMethod.DELETE, new HttpEntity<>(headers), String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.FORBIDDEN);
    }
}
