package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
public class DemoController {

    private final Logger log = LoggerFactory.getLogger(DemoController.class);

    private String planetName = "world";

    @PostMapping("/hello")
    public void changeTheWorld(@RequestParam final String planetName) {
        this.planetName = planetName;
    }

    @DeleteMapping("/hello")
    public void destroyPlanet() {
        planetName = "";
    }

    @GetMapping("/hello")
    public String helloWorld(@RequestHeader final Map<String, String> headers, final Principal principal) {
        log.debug("Received {}", principal);
        log.debug("Headers are {}", headers);
        return "Hello " + planetName + ": " + principal;
    }
}
