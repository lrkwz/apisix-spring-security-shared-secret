package com.example.demo.security;

import io.micrometer.common.util.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RequestHeaderAuthenticationProvider implements AuthenticationProvider {
    private final Logger log = LoggerFactory.getLogger(RequestHeaderAuthenticationProvider.class);

    @Value("${api.auth.secret}")
    private String apiAuthSecret;

    @Override
    public Authentication authenticate(final @NotNull Authentication authentication) throws AuthenticationException {
        final String authSecretKey = String.valueOf(authentication.getPrincipal());
        log.debug("authenticate received auth secret key: {} to be compared with {}", authSecretKey, apiAuthSecret);

        if (StringUtils.isBlank(authSecretKey) || !authSecretKey.equals(apiAuthSecret)) {
            throw new BadCredentialsException("Bad Request Header Credentials");
        }

        final User user = new User((String) authentication.getPrincipal(), "", List.of(new SimpleGrantedAuthority("EDITOR"), new SimpleGrantedAuthority("USER")));
        return new PreAuthenticatedAuthenticationToken(user, null, user.getAuthorities());
    }

    @Override
    public boolean supports(final Class<?> authentication) {
        return authentication.equals(PreAuthenticatedAuthenticationToken.class);
    }
}
