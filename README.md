# Shared secret key authentication with Apache APISix and Spring boot

Using the advice
from
Baeldung's [Shared Secret Authentication in Spring Boot Application](https://www.baeldung.com/spring-boot-shared-secret-authentication)
here is the integration with [Apache APISix](https://apisix.apache.org/).

For sake of simplicity APISIX is deployed in standalone mode (see `apisix-config.yml`).

Long story short:

* Apache APISix accepts an apikey to identify a consumer (see plugins `key-auth` and `consumer-restriction`
  in `apisix.yml`).
* Apache APISix forwards the `consumer_name` (we really do not need to propagate the apikey) to the backend service in
  the http header named `x-auth-secret-key`
* Spring Security is configured in order to assume that che request has already been authenticated by Apache APISix.

## Running the sample code

Execute `mvn spring-boot:run` in order to launch the backend service.

Execute `docker-compose up -d` in order to laung Apache APISix.

Then running:

* `http :9080/hello` will lead to a `401 Unauthorized` response,
* `http :9080/health`will reply `OK`
* `http :9080/hello apikey==0123456789` or `http :9080/hello apikey:0123456789`[^1] replies with the
  AuthenticationToken
  info having identified the APISix consumer name as the Spring Security context's `Principal`

[^1]: If you are not familiar with [HTTPie](https://httpie.io) using `==` builds a query parameter based request and `:`
injects apikey ad a custom http header.
